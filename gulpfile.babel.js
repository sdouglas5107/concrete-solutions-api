'use strict';

import _ from "lodash";
import del from "del";
import gulp from "gulp";
import gulpLoadPlugins from "gulp-load-plugins";
import lazypipe from "lazypipe";
import nodemon from "nodemon";
import runSequence from "run-sequence";

var plugins = gulpLoadPlugins();
var config;
const serverPath = 'server';
const paths = {
  server: {
    json: [`${serverPath}/**/*.json`],
    test: {
      unit: [`${serverPath}/**/*.spec.js`]
    }
  },
  karma: 'karma.conf.js',
  dist: 'dist'
};

/********************
 * Helper functions
 ********************/

function onServerLog(log) {
  console.log(plugins.util.colors.white('[') +
    plugins.util.colors.yellow('nodemon') +
    plugins.util.colors.white('] ') +
    log.message);
}

let mocha = lazypipe()
  .pipe(plugins.mocha, {
    reporter: 'spec',
    timeout: 5000,
    require: [
      './mocha.conf'
    ]
  });

/********************
 * Env
 ********************/

gulp.task('env:all', () => {
  plugins.env({
    vars: {}
  });
});
gulp.task('env:test', () => {
  plugins.env({
    vars: {NODE_ENV: 'test'}
  });
});
gulp.task('env:prod', () => {
  plugins.env({
    vars: {NODE_ENV: 'production'}
  });
});

/********************
 * Tasks
 ********************/

gulp.task('clean:tmp', () => del(['.tmp/**/*'], {dot: true}));

gulp.task('start:server', () => {
  process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  config = require(`./${serverPath}/config/environment`);
  nodemon(`-w ${serverPath} ${serverPath}`)
    .on('log', onServerLog);
});

gulp.task('start:inspector', () => {
  gulp.src([])
    .pipe(plugins.nodeInspector());
});

gulp.task('watch', () => {
  var testFiles = _.union(paths.server.test.unit);
  plugins.livereload.listen();
  plugins.watch(_.union(paths.server.scripts, testFiles))
    .pipe(plugins.plumber())
    .pipe(plugins.livereload());
});

gulp.task('serve', cb => {
  runSequence(['clean:tmp', 'env:all'],
    ['start:inspector','start:server'],
    'watch',
    cb);
});

gulp.task('test', cb => {
  return runSequence('test:server', cb);
});

gulp.task('test:server', cb => {
  runSequence(
    'env:all',
    'env:test',
    'mocha:unit',
    cb);
});

gulp.task('mocha:unit', () => {
  return gulp.src(paths.server.test.unit)
    .pipe(mocha());
});

/********************
 * Build
 ********************/

gulp.task('build', cb => {
  runSequence(
    [
      'clean:tmp'
    ],
    [
      'transpile:server'
    ],
    cb);
});
