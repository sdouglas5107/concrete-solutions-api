## Getting Started

### Prerequisites

- [Node.js and npm](nodejs.org) Node ^4.2.3, npm ^2.14.7
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [MongoDB](https://www.mongodb.org/) - Keep a running daemon with `mongod`

### Developing

1. Run `npm install` to install server dependencies.

2. Run `gulp serve` to start the development server

### Testing

Running `gulp test` will run the unit tests with karma.

### About It

#### Routes

- `POST /api/users` Creates a new user
- `GET /api/users` Gets the logged user, should have the `Authorization` header
- `POST /api/auth` Authenticates the user

#### Where is It?

It is running on AWS responding on address http://52.36.78.239:9000/
