import passport from "passport";
import {Strategy as LocalStrategy} from "passport-local";

function localAuthenticate(User, email, password, done) {
  User.findOne({
    email: email.toLowerCase()
  }).exec()
    .then(user => {
      if (!user) {
        return done(null, false, {
          message: 'Usuário e/ou senha inválidos'
        });
      }
      user.authenticate(password, function (authError, authenticated) {
        if (authError) {
          return done(authError);
        }
        if (!authenticated) {
          return done(null, false, {message: 'Usuário e/ou senha inválidos'});
        } else {
          return done(null, user);
        }
      });
    })
    .catch(err => done(err));
}

export function setup(User) {
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'senha'
  }, function (email, password, done) {
    return localAuthenticate(User, email, password, done);
  }));
}
