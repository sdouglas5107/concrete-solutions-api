'use strict';

import express from "express";
import config from "../config/environment";
import User from "../api/user/user.model";
import passport from "passport";
import {signToken} from "./auth.service";

// Passport Configuration
require('./passport').setup(User, config);

var router = express.Router();

router.post('/', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    err = err || info;
    if (err) {
      var statusCode = 401;
      if (err.message && err.message == "Missing credentials") {
        return res.status(statusCode).json({mensagem: "Usuário e/ou senha inválidos"});
      } else if (err.message) {
        return res.status(statusCode).json({mensagem: err.message});
      } else {
        return res.status(statusCode).json({mensagem: "Algo inesperado aconteceu"});
      }
    }
    if (!user) {
      return res.status(404).json({mensagem: "Usuário não encontrado"});
    }
    user.token = signToken(user._id);
    user.save().then(user => {
      res.json(user.toJson());
    });
  })(req, res, next)
});

module.exports = router;
