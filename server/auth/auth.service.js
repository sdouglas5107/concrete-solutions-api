'use strict';

import config from "../config/environment";
import jwt from "jsonwebtoken";
import expressJwt from "express-jwt";
import compose from "composable-middleware";
import User from "../api/user/user.model";

var validateJwt = expressJwt({
  secret: config.secrets.session
});

export function isAuthenticated() {
  return compose()
  // Validate jwt
    .use(function (req, res, next) {
      validateJwt(req, res, function (err) {
        if (err) {
          err = JSON.stringify(err);
        }
        //Caso não seja a MENOS que 30 minutos atrás, retornar erro com status apropriado com mensagem "Sessão inválida".
        if (err && err.includes("jwt expired")) {
          return res.status(401).json({mensagem: "Sessão inválida"});
        }
        //Caso o token não exista, retornar erro com status apropriado com a mensagem "Não autorizado".
        //Caso não seja o mesmo token, retornar erro com status apropriado e mensagem "Não autorizado"
        if (err && err.includes("UnauthorizedError")) {
          return res.status(401).json({mensagem: "Não autorizado"});
        }
        //Has not errors, then will call next method to return the user.
        next();
      });
    })
    // Attach user to request
    .use(function (req, res, next) {
      User.findById(req.user._id).exec()
        .then(user => {
          if (!user) {
            return res.status(401).end();
          }
          req.user = user;
          next();
        })
        .catch(err => next(err));
    });
}

/**
 * Returns a jwt token signed by the app secret
 */
export function signToken(id) {
  return jwt.sign({_id: id}, config.secrets.session, {
    expiresIn: "30m"//Caso seja o mesmo token, verificar se o último login foi a MENOS que 30 minutos atrás.
  });
}
