'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var userCtrlStub = {
  me: 'userCtrl.me',
  create: 'userCtrl.create'
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  }
};

var routerStub = {
  get: sinon.spy(),
  post: sinon.spy()
};

// require the index with our stubbed out modules
var userIndex = proxyquire('./index', {
  'express': {
    Router() {
      return routerStub;
    }
  },
  './user.controller': userCtrlStub,
  '../../auth/auth.service': authServiceStub
});

describe('User API Router:', function () {

  it('should return an express router instance', function () {
    userIndex.should.equal(routerStub);
  });

  describe('GET /api/users/', function () {

    it('should be authenticated and route to user.controller.me', function () {
      routerStub.get
        .withArgs('/', 'authService.isAuthenticated', 'userCtrl.me')
        .should.have.been.calledOnce;
    });

  });


  describe('POST /api/users', function () {
    it('should route to user.controller.create', function () {
      routerStub.post
        .withArgs('/', 'userCtrl.create')
        .should.have.been.calledOnce;
    });
  });

});
