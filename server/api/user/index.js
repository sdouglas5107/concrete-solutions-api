'use strict';

import {Router} from "express";
import * as controller from "./user.controller";
import * as auth from "../../auth/auth.service";

var router = new Router();
//Chamadas para este endpoint devem conter um header na requisição de Authentication com o valor "Bearer {token}"
// onde {token} é o valor do token passado na criação ou sign in de um usuário.
//OBSERVAÇÃO: Authentication não é um header válido. portanto o Header deve er Authorization
router.get('/', auth.isAuthenticated(), controller.me);
router.post('/', controller.create);

module.exports = router;
