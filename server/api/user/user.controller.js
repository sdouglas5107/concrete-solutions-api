'use strict';

import User from "./user.model";
import {signToken} from "../../auth/auth.service";

function handleError(res, statusCode) {
  statusCode = statusCode || 500;//Internal Server Error
  return function () {
    res.status(statusCode).send({mensagem: "Erro inesperado no servidor"});
  };
}

/**
 * Creates a new user
 */
export function create(req, res) {
  var newUser = new User(req.body);
  newUser.create(function (err, user) {
    if (err) {
      var statusCode = 422;//Unprocessable Entity
      if (err.errors && err.errors[Object.keys(err.errors)[0]]) {
        return res.status(statusCode).json({mensagem: err.errors[Object.keys(err.errors)[0]].message});
      } else {
        return res.status(statusCode).json({mensagem: "Algo inesperado aconteceu"});
      }
    }
    user.token = signToken(user._id);
    user.save().then(user => {
      res.status(201).json(user.toJson());
    });
  });
}

/**
 * Get my info
 */
export function me(req, res) {
  var user = req.user;
  if (!user) {
    return res.status(401).json({mensagem: "Usuário não autorizado"});
  }
  res.json(user.toJson());
}
