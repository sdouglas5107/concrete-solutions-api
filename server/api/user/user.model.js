'use strict';

import crypto from "crypto";
import mongoose, {Schema} from "mongoose";
mongoose.Promise = require('bluebird');

var UserSchema = new Schema({
  nome: {type: String, required: true},
  email: {
    type: String,
    lowercase: true
  },
  senha: String,
  data_criacao: {type: Date, default: Date.now},//data da criação do usuário
  data_atualizacao: Date, //data da última atualização do usuário
  ultimo_login: Date, // data do último login (no caso da criação, será a mesma que a criação)
  telefones: Array,
  token: String,//O token deverá ser persistido junto com o usuário
  salt: String
});// id do usuário (pode ser o próprio gerado pelo banco, porém seria interessante se fosse um GUID)


/**
 * Validations
 */

// Validate empty email
UserSchema
  .path('email')
  .validate(function (email) {
    return email.length;
  }, 'Email não pode ser vazio');

// Validate empty password
UserSchema
  .path('senha')
  .validate(function (password) {
    return password.length;
  }, 'Senha não pode ser vazia');

// Validate email is not taken
UserSchema
  .path('email')
  .validate(function (value, respond) {
    var self = this;
    return this.constructor.findOne({email: value}).exec()
      .then(function (user) {
        if (user) {
          if (self.id === user.id) {
            return respond(true);
          }
          return respond(false);
        }
        return respond(true);
      })
      .catch(function (err) {
        throw err;
      });
  }, 'E-mail já existente');

var validatePresenceOf = function (value) {
  return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
  .pre('save', function (next) {
    if (this.isModified('token')) {
      this.ultimo_login = new Date();// data do último login (no caso da criação, será a mesma que a criação)
    } else {
      this.data_atualizacao = new Date();//data da última atualização do usuário
    }
    return next();
  });

/**
 * Methods
 */
UserSchema.methods = {

  create(callback){
    var user = this;
    if (!validatePresenceOf(user.senha)) {
      return next(new Error('Senha não pode ser vazia'));
    }
    user.makeSalt((saltErr, salt) => {
      if (saltErr) {
        return callback(saltErr);
      }
      user.salt = salt;
      user.encryptPassword(user.senha, (encryptErr, hashedPassword) => {
        if (encryptErr) {
          return callback(encryptErr);
        }
        user.senha = hashedPassword;
        user.save().then(user => {
          callback(null, user)
        }).catch(err => {
          callback(err);
        });
      });
    });
  },

  toJson () {
    var obj = this.toObject();
    //Rename id
    obj.id = obj._id;
    delete obj._id;
    delete obj.__v;
    // don't ever give out the password or salt
    delete obj.senha;
    delete obj.salt;
    return obj;
  },

  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} password
   * @param {Function} callback
   * @return {Boolean}
   * @api public
   */
  authenticate(password, callback) {
    if (!callback) {
      return this.senha === this.encryptPassword(password);
    }

    this.encryptPassword(password, (err, pwdGen) => {
      if (err) {
        return callback(err);
      }

      if (this.senha === pwdGen) {
        callback(null, true);
      } else {
        callback(null, false);
      }
    });
  },

  /**
   * Make salt
   *
   * @param {Number} byteSize Optional salt byte size, default to 16
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  makeSalt(byteSize, callback) {
    var defaultByteSize = 16;

    if (typeof arguments[0] === 'function') {
      callback = arguments[0];
      byteSize = defaultByteSize;
    } else if (typeof arguments[1] === 'function') {
      callback = arguments[1];
    }

    if (!byteSize) {
      byteSize = defaultByteSize;
    }

    if (!callback) {
      return crypto.randomBytes(byteSize).toString('base64');
    }

    return crypto.randomBytes(byteSize, (err, salt) => {
      if (err) {
        callback(err);
      } else {
        callback(null, salt.toString('base64'));
      }
    });
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @param {Function} callback
   * @return {String}
   * @api public
   */
  encryptPassword(password, callback) {
    if (!password || !this.salt) {
      if (!callback) {
        return null;
      } else {
        return callback('Missing password or salt');
      }
    }

    var defaultIterations = 10000;
    var defaultKeyLength = 64;
    var salt = new Buffer(this.salt, 'base64');

    if (!callback) {
      return crypto.pbkdf2Sync(password, salt, defaultIterations, defaultKeyLength)
        .toString('base64');
    }

    return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength, (err, key) => {
      if (err) {
        callback(err);
      } else {
        callback(null, key.toString('base64'));
      }
    });
  }
};

export default mongoose.model('User', UserSchema);
