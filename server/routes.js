/**
 * Main application routes
 */

'use strict';

export default function (app) {
  // Insert routes below
  app.use('/api/users', require('./api/user'));
  app.use('/api/auth', require('./auth'));

  //All non defined routes
  //O servidor deverá retornar JSON para os casos de endpoint não encontrado também.
  app.all('/*', (req, res) => {
    res.status(404).json({"mensagem": "Recurso não encontrado"});
  });
}
